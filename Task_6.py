from datetime import date


class MyClass1:
    def __init__(self, surname, name, age):
        self.surname = surname
        self.name = name
        self.age = age

    @classmethod
    def fromBirthYear(cls, surname, name, birthYear):
        return cls(surname, name, date.today().year - birthYear)

    def print_info(self):
        print(self.surname + " " + self.name + "'s age is: " + str(self.age))

    def is_adult(self):
        if self.age >= 18:
            return True
        else:
            return False

    # New code
    @classmethod
    def count_adult(cls):
        data = []
        count_adult_person = 0

        data.append(MyClass1("Poroshenko", "Petro", 12))
        data.append(MyClass1("Sergijenko", "Vasyl", 19))
        data.append(MyClass1("Tyshscenko", "Maksym", 29))
        data.append(MyClass1("Klarko", "Sergiy", 10))
        data.append(MyClass1("Kimchenko", "Smit", 56))

        for person in data:
            if person.is_adult():
                count_adult_person = count_adult_person + 1
        return count_adult_person

class MyClass2(MyClass1):
    color = 'White'


m_per1 = MyClass1('Ivanenko', 'Ivan', 19)
m_per1.print_info()

m_per2 = MyClass1.fromBirthYear('Dovzhenko', 'Bogdan',  2000)
m_per2.print_info()

m_per3 = MyClass2.fromBirthYear('Sydorchuk', 'Petro', 2010)
print(isinstance(m_per3, MyClass2))

m_per4 = MyClass2.fromBirthYear('Makuschenko', 'Dmytro', 2001)
print(isinstance(m_per4, MyClass1))

print(issubclass(MyClass1, MyClass2))
print(issubclass(MyClass2, MyClass1))

#New code
print(MyClass1.count_adult())

