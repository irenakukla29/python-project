

class Editor (object):
    password = input('Enter the license key from the keyboard: ')

    def __new__(cls):
        if Editor.password == 'pro':
            print("Your license key is valid till 22/03/2023!")
            return ProEditor()
        else:
            print("Your license key is not valid!")
            return object.__new__(cls)

    def __init__(self, document='Empty Document'):
        self.document = document

    def view_document(self):
        print(self.document)

    def edit_document(self):
        print("Document editing is not available for the FREE version!")


class ProEditor (Editor):

    def __new__(cls):
        return object.__new__(cls)

    def __init__(self, document='Empty Document'):
        super().__init__(document)
        self.document = document

    def view_document(self):
        print(self.document)

    def edit_document(self):
        print("Save the changes to a new document:")
        new_ver = input()
        self.document = new_ver
        print(f"Document is changed: {self.document}")


editor = Editor()
editor.view_document()
editor.edit_document()
