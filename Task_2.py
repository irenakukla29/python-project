import tkinter as tk


class Rectangle:
    def __init__(self, width=20, height=7):
        self.width = width
        self.height = height

    def area(self):
        return str(f"{self.width}x{self.height}")


class Button(Rectangle):

    def click_button(self):

        # create root window
        root = tk.Tk()
        # root window title and dimension
        root.title("Welcome")
        root.geometry(Rectangle.area(self))

        # creating button
        btn = tk.Button(root, text="Press")
        btn.pack()
        # running the main loop
        root.mainloop()


Button(50, 200).click_button()