class Vehicle:
    def __init__(self, price, color, style, brand):
        self.price = price
        self.color = color
        self.style = style
        self.brand = brand


class Truck(Vehicle):
    def __init__(self, price, color, style, brand, wheels):
        super().__init__(price, color, style, brand)
        self.wheels = wheels

    def wheels_number(self):
        return f"The truck has a {self.wheels} wheels "


class Car(Vehicle):
    def __init__(self, price, color, style, brand, speed):
        super().__init__(price, color, style, brand)
        self.speed = speed

    def car_speed(self):
        return f"The SUV has a maximum speed {self.speed}"


truck = Truck(150.000, 'Black', 'Truck', 'Mercedes', 10)
car = Car()

print(truck.wheels_number())

