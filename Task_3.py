class Worker:
    title = ''
    pay = ''

    def set_title(self, title):
        self.title = title

    def set_pay(self, pay):
        self.pay = pay


class TeamMember:
    project = ''

    def set_project(self, project):
        self.project = project


class TeamLeader(TeamMember, Worker):
    experience = ''


employee = TeamLeader()
employee.set_title("Team Lead")
employee.set_pay(900000)
employee.set_project("Python Zen")

print(TeamLeader.mro())

#Пояснення
"""Ми повернули список з методу mro(), клас TeamLeader у такому випадку з'явився
перед батьківськими, а далі за наслідуваністю."""